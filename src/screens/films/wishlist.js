import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Title from "../../components/title";
import Button from "../../components/button";
import styled from "styled-components";

const Wishlist = () => {
  const [wishlist, setWishlist] = useState([]);

  useEffect(() => {
    setWishlist(JSON.parse(localStorage.getItem("Wishlist")) || []);
  }, []);

  const removeWishlist = (idFilm) => {
    let wl = JSON.parse(localStorage.getItem("Wishlist")) || [];
    const totaux = wl.filter((f) => f.id !== idFilm);
    localStorage.setItem("Wishlist", JSON.stringify(totaux ?? []));
    setWishlist(totaux);
  };
  const emptyWishlist = () => {
    localStorage.setItem("Wishlist", JSON.stringify([]));
    setWishlist([]);
  };
  return (
    <div>
      <Title title={"Liste de souhaits"}></Title>
      <WishlistContainer>
        {wishlist[0] ? (
          <StyledTable>
            <thead>
              <tr>
                <th>VO</th>
                <th>Nom</th>
                <th>Note moyenne</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {wishlist.map((film) => {
                return (
                  <tr>
                    <td>{film.original_language}</td>
                    <td>{film.title}</td>
                    <td>{film.vote_average}</td>
                    <td>
                      <Link to={`/film/${film.id}`}>
                        <Button
                          title={"Détails"}
                          classes={"btn btn-blue"}
                        ></Button>
                      </Link>
                      <Button
                        classes={"btn btn-red"}
                        title={"Retirer"}
                        func={() => removeWishlist(film.id)}
                      ></Button>
                    </td>
                  </tr>
                );
              })}
              <tr>
                <td colSpan={3}></td>
                <td>
                  <Button
                    classes={"btn btn-red"}
                    title="Vider la liste de souhaits"
                    func={() => emptyWishlist()}
                  ></Button>
                </td>
              </tr>
            </tbody>
          </StyledTable>
        ) : (
          <p>La liste de souhaits est vide</p>
        )}
      </WishlistContainer>
    </div>
  );
};

const StyledTable = styled.table`
  width: 100%;

  th,
  td {
    height: 50px;
    border-bottom: 1px solid #ddd;
    padding: 3px;
    text-align: left;
  }
  tbody tr {
    &:hover {
      background-color: #f5f5f5;
    }
  }
`;

const WishlistContainer = styled.div``;

export default Wishlist;

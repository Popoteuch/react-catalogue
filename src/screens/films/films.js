import React, { useState, useEffect } from "react";
import axios from "axios";
import Title from "../../components/title";
import { Grid } from "../../components/film";

const Films = (props) => {
  const [filmsList, setFilmsList] = useState([]);
  useEffect(() => {
    const api_key = "a5fa556963902c2c8c768941f0e54f3c";
    const generatedUrl = `https://api.themoviedb.org/3/movie/${props.match.params.type}`;
    axios({
      method: "GET",
      url: generatedUrl,
      params: {
        language: "fr-FR",
        api_key: api_key,
      },
    })
      .then((res) => {
        setFilmsList(res.data.results);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [props.match.params.type]);
  let title;
  switch (props.match.params.type) {
    case "upcoming":
      title = "Films à venir";
      break;
    case "popular":
      title = "Films populaires";
      break;
    case "top_rated":
      title = "Films les mieux notés";
      break;
    default:
      title = "Films";
      break;
  }
  return (
    <div>
      <Title title={title}></Title>
      <Grid films={filmsList}></Grid>
    </div>
  );
};

export default Films;

import React, { useState, useEffect } from "react";
import axios from "axios";
import Title from "../../components/title";
import { Card } from "../../components/film";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import Loader from "react-loader-spinner";
import styled from "styled-components";

const Film = (props) => {
  const [film, setFilm] = useState({});
  useEffect(() => {
    const api_key = "a5fa556963902c2c8c768941f0e54f3c";
    const generatedUrl = `https://api.themoviedb.org/3/movie/${props.match.params.id}`;
    axios({
      method: "GET",
      url: generatedUrl,
      params: {
        language: "fr-FR",
        api_key: api_key,
      },
    })
      .then((res) => {
        setFilm(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  return (
    <div>
      <Title title={"Film"}></Title>
      {Object.keys(film).length !== 0 || film.constructor !== Object ? (
        <Card film={film} details={true}></Card>
      ) : (
        <DivCentree>
          <Loader
            type={"Oval"}
            color={"#00BFFF"}
            height={100}
            width={100}
          ></Loader>
          {"Chargement en cours..."}
        </DivCentree>
      )}
    </div>
  );
};

const DivCentree = styled.div`
  text-align: center;
`;

export default Film;

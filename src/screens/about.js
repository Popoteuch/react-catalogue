import React from "react";

import Title from "../components/title";

const About = () => {
  return (
    <div>
      <Title title={"À propos"}></Title>
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore iure
      nihil quas quos! A accusantium aliquam, aperiam earum fuga molestiae
      obcaecati reprehenderit vitae voluptate voluptatem? Aut eveniet quae ut
      veritatis.
    </div>
  );
};

export default About;

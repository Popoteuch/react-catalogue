import React, { useState, useEffect } from "react";
import axios from "axios";
import Title from "../components/title";
import { Card } from "../components/film/";
import styled from "styled-components";

const Home = (props) => {
  const [film, setFilm] = useState({});
  useEffect(() => {
    const api_key = "a5fa556963902c2c8c768941f0e54f3c";
    const generatedUrl = `https://api.themoviedb.org/3/movie/${Math.trunc(
      Math.random() * 100000
    )}`;
    axios({
      method: "GET",
      url: generatedUrl,
      params: {
        language: "fr-FR",
        api_key: api_key,
      },
    })
      .then((res) => {
        setFilm(res.data);
      })
      .catch((err) => {
        console.log("Pas de film à l'id aléatoire générée");
      });
  }, []);
  return (
    <div>
      <Title title={"Home"}></Title>
      <StyledP>Bienvenue sur notre site consacré au 7ème art !</StyledP>
      <StyledP>
        Ici vous pourrez trouver votre bonheur pour créer une liste de films à
        regarder pour ne plus jamais être à cours d'idées.
      </StyledP>
      {film.id ? (
        <div>
          <StyledP>
            Et puisque le hasard fait bien les choses, voici un film aléatoire
            de notre catalogue.
          </StyledP>
          <Card film={film} details={true}></Card>
        </div>
      ) : (
        <StyledP>Revenez plus tard pour une surprise</StyledP>
      )}
    </div>
  );
};

const StyledP = styled.p`
  width: 100%;
`;

export default Home;

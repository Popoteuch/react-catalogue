import React from "react";

import Title from "../components/title";

const Contact = () => {
  return (
    <div>
      <Title title={"Contact"}></Title>
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam,
      asperiores atque autem commodi dolores ducimus earum enim error expedita
      fugit illum iste possimus quae quasi quia, saepe, tempora ut veniam!
    </div>
  );
};

export default Contact;

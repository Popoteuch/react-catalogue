import React from "react";
import styled, { ThemeProvider } from "styled-components";
import "./App.css";
import Header from "./components/header";
import Footer from "./components/footer";
import Routes from "./config/router";
import { theme } from "./config/theme";

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Header></Header>
      <RoutesContainer>
        <Routes></Routes>
      </RoutesContainer>
      <Footer></Footer>
    </ThemeProvider>
  );
}

const RoutesContainer = styled.div`
  padding-left: 30px;
  padding-right: 30px;
`;

export default App;

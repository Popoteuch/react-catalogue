import React from "react";
import PropTypes from "prop-types";

const Affichage = (props) => {
  return (
    <div>
      <label htmlFor={`${props.object}_${props.element}_${props.id}`}>
        {props.title} :{" "}
      </label>
      <span id={`${props.object}_${props.element}_${props.id}`}>
        {props.value}
      </span>
    </div>
  );
};
Affichage.propsTypes = {
  element: PropTypes.string.isRequired,
  object: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
};

export default Affichage;

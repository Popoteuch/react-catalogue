import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const Button = (props) => {
  return (
    <ButtonContainer>
      <button className={props.classes} onClick={props.func}>
        {props.title}
      </button>
    </ButtonContainer>
  );
};

const ButtonContainer = styled.div`
  .btn {
    border-color: limegreen;
    color: limegreen;
    background-color: white;
    border-radius: 5px;
    min-width: 80px;
    min-height: 30px;
    &:hover {
      border-color: white;
      color: white;
      background-color: limegreen;
    }
    &-blue {
      border-color: #33afff;
      color: #33afff;
      background-color: white;
      &:hover {
        border-color: white;
        color: white;
        background-color: #33afff;
      }
    }
    &-red {
      border-color: #fa1111;
      color: #fa1111;
      background-color: white;
      &:hover {
        border-color: white;
        color: white;
        background-color: #fa1111;
      }
    }
  }

  .btn-close {
    border: none;
    font-size: 20px;
    padding: 20px;
    cursor: pointer;
    font-weight: bold;
    color: #4aae9b;
    background: transparent;
  }
`;

Button.defaultProps = {
  classes: "btn",
};

Button.propsTypes = {
  classes: PropTypes.string,
  func: PropTypes.func,
  title: PropTypes.string,
};

export default Button;

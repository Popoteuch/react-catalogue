import React from "react";
import Card from "./card";
import styled from "styled-components";
import Loader from "react-loader-spinner";

const Grid = (props) => {
  const films = props.films;
  let cards = [];
  films.forEach((film, idFilm) => {
    cards[idFilm] = <Card details={false} film={film}></Card>;
  });
  return films.length !== 0 ? (
    <FilmGrid>{cards}</FilmGrid>
  ) : (
    <DivCentree>
      <Loader type={"Oval"} color={"#00BFFF"} height={100} width={100}></Loader>
      {"Chargement en cours..."}
    </DivCentree>
  );
};

const FilmGrid = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(300px, 1fr));
`;

const DivCentree = styled.div`
  text-align: center;
`;

export default Grid;

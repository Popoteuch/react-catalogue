import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import LabelAffichage from "../label/affichage";
import Button from "../button";
import styled from "styled-components";

const Card = (props) => {
  const [wishlist, setWishlist] = useState([]);
  const [displayBtn, setDisplayBtn] = useState(false);
  useEffect(() => {
    setWishlist(JSON.parse(localStorage.getItem("Wishlist")) || []);
  }, []);
  useEffect(() => {
    addPossible();
  }, [wishlist, props.film]);

  const film = props.film;
  const path = "https://image.tmdb.org/t/p/w500/";
  const addPossible = () => {
    const index = wishlist
      .map((fi) => {
        return fi.id;
      })
      .indexOf(props.film.id);
    setDisplayBtn(index === -1);
  };
  const addWishlist = () => {
    let wl = wishlist || [];
    const index = wl
      .map((fi) => {
        return fi.id;
      })
      .indexOf(film.id);
    if (index === -1) {
      wl.push(film);
    }
    localStorage.setItem("Wishlist", JSON.stringify(wl));
    setWishlist(wl);
    setDisplayBtn(false);
  };
  const film_content = () => {
    let res;
    if (props.details) {
      res = (
        <FilmContent>
          <h2>{film.title}</h2>
          <FilmBody>
            <FilmImage id={film.id}>
              <img src={path + film.poster_path} alt="" />
            </FilmImage>
            <FilmCaracteristiques id={film.id}>
              <LabelAffichage
                element={"film"}
                object={"overview"}
                id={film.id}
                title={"Résumé"}
                value={film.overview}
              ></LabelAffichage>
              <LabelAffichage
                element={"film"}
                object={"vote-average"}
                id={film.id}
                title={"Note moyenne"}
                value={film.vote_average}
              ></LabelAffichage>
              <LabelAffichage
                element={"film"}
                object={"release-date"}
                id={film.id}
                title={"Date de sortie"}
                value={film.release_date}
              ></LabelAffichage>
              {displayBtn ? (
                <Button
                  title={"Ajouter à la liste de souhaits"}
                  func={() => addWishlist(film)}
                ></Button>
              ) : (
                <PDejaAjoute>
                  Ce film est dans votre liste de souhaits ✅
                </PDejaAjoute>
              )}
            </FilmCaracteristiques>
          </FilmBody>
        </FilmContent>
      );
    } else {
      res = (
        <FilmContent>
          <FilmBody>
            <FilmImage>
              <img src={path + film.poster_path} alt="" />
            </FilmImage>
            <div>
              <Link to={`/film/${film.id}`}>
                <Button title={"Détails"} classes={"btn btn-blue"}></Button>
              </Link>
            </div>
          </FilmBody>
        </FilmContent>
      );
    }
    return res;
  };
  return film_content();
};

const FilmContent = styled.div`
  box-shadow: 1px 1px 5px 1px #8080805c;
  margin: 5px;
  border-radius: 10px;
  text-align: center;
  padding: 5px 5px 5px 5px;
`;

const FilmBody = styled.div`
  display: inline-block;
`;

const FilmImage = styled.div`
  & img {
    border-radius: 8px;
    max-width: 300px;
    max-height: 300px;
  }
`;

const FilmCaracteristiques = styled.div`
  & div {
    padding: 10px;
  }
`;

const PDejaAjoute = styled.p`
  color: green;
`;

Card.propsTypes = {
  film: PropTypes.object.isRequired,
  details: PropTypes.bool,
};
Card.defaultProps = {
  details: false,
};

export default Card;

import React, { useState, useEffect } from "react";
import { Link, useHistory } from "react-router-dom";
import styled from "styled-components";

const Footer = () => {
  const history = useHistory();
  const [location, setLocation] = useState([]);
  useEffect(() => {
    setLocation(history.location.pathname);
  }, []);
  const handleClick = (path) => {
    history.push(path);
    setLocation(path);
  };
  return (
    <footer>
      <StyledNav>
        <StyledLink
          onClick={() => handleClick("/about")}
          selected={location === "/about"}
        >
          À propos
        </StyledLink>
        <StyledLink
          onClick={() => handleClick("/contact")}
          selected={location === "/contact"}
        >
          Contact
        </StyledLink>
      </StyledNav>
    </footer>
  );
};

const StyledNav = styled.div`
  padding: 30px;
  display: flex;
  justify-content: center;
`;
const StyledLink = styled(Link)`
  cursor: pointer;
  color: ${(props) => (props.selected ? "#42b983" : "#2c3e50")};
  padding: 10px;
  font-weight: bold;
  :hover {
    color: #42b983;
  }
`;

export default Footer;

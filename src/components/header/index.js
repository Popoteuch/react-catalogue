import React, { useState, useEffect } from "react";
import { Link, useHistory } from "react-router-dom";
import styled from "styled-components";

const Header = (props) => {
  const history = useHistory();
  const [location, setLocation] = useState([]);
  useEffect(() => {
    setLocation(history.location.pathname);
  }, []);
  const handleClick = (path) => {
    history.push(path);
    setLocation(path);
  };
  const menus = {
    "/": "Accueil",
    "/films/upcoming": "Films à venir",
    "/films/popular": "Films populaires",
    "/films/top_rated": "Films les mieux notés",
    "/wishlist": "Liste de souhaits",
  };
  let links = [];
  for (const [lien, nom] of Object.entries(menus)) {
    links.push(
      <StyledLink
        onClick={() => handleClick(lien)}
        selected={location === lien}
      >
        {nom}
      </StyledLink>
    );
  }
  return (
    <header>
      <StyledNav>{links}</StyledNav>
    </header>
  );
};

const StyledLink = styled(Link)`
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
  :hover {
    background-color: #ddd;
    color: black;
  }

  background-color: ${(props) => (props.selected ? "#4CAF50" : "#333")};
  color: ${(props) => (props.selected ? "white" : "#f2f2f2")};
`;

const StyledNav = styled.div`
  background-color: #333;
  overflow: hidden;

  @media screen and (max-width: 600px) {
    position: relative;
  }
`;

export default Header;

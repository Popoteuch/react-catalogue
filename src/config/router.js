import React from "react";
import { Route, Redirect, Switch } from "react-router-dom";
import Home from "../screens/home";
import About from "../screens/about";
import Contact from "../screens/contact";
import Films from "../screens/films/films";
import Film from "../screens/films/film";
import Wishlist from "../screens/films/wishlist";

const Routes = () => {
  return (
    <Switch>
      <Route exact path={"/"} component={Home}></Route>
      <Route path={"/home"} component={Home}></Route>
      <Route path={"/about"} component={About}></Route>
      <Route path={"/contact"} component={Contact}></Route>
      <Route path={"/films/:type"} component={Films}></Route>
      <Route path={"/film/:id"} component={Film}></Route>
      <Route path={"/wishlist"} component={Wishlist}></Route>
      <Redirect to={"/"}></Redirect>
    </Switch>
  );
};

export default Routes;
